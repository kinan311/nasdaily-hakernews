
import React from "react";
import Headroom from "headroom.js";
import {
  NavbarBrand,
  Navbar
} from "reactstrap";

class DemoNavbar extends React.Component {
  componentDidMount() {
    let headroom = new Headroom(document.getElementById("navbar-main"));
    // initialise
    headroom.init();
  }
  state = {
    collapseClasses: "",
    collapseOpen: false
  };

  onExiting = () => {
    this.setState({
      collapseClasses: "collapsing-out"
    });
  };

  onExited = () => {
    this.setState({
      collapseClasses: ""
    });
  };

  render() {
    return (
      <>
        <header className="header-global">
          <Navbar
            className=" navbar-transparent navbar-light "
            expand="lg"
            id="navbar-main"
          >
           
              <NavbarBrand className="mr-lg-12" to="/" >
                <h4 className="primary-text">HACKER<span className="secondary-text">NEWS</span>.</h4>
              </NavbarBrand>
           
          </Navbar>
        </header>
      </>
    );
  }
}

export default DemoNavbar;
