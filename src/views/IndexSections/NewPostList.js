/* eslint-disable */
import React ,{memo, useEffect , useState} from "react";
import { connect } from 'react-redux';
import {getNewPosts , getPostsById, clearMessages} from '../../actions'
import FlatList from 'flatlist-react';
import {slice} from 'lodash';
import Skeleton , { SkeletonTheme }from 'react-loading-skeleton';

import {
  Button,
  Spinner,
  Row,
  Alert
} from "reactstrap";
import Post from "./Post";




const NewPostList = (props) => {

  const limit = 5;
  const [startIndex , setStartInsdex ] = useState(0);
  const [postsLenght , setPostsLenght ] = useState(5);
  const [newPostsView , setNewPostsView] = useState([])

  const[postIds , setPostIds] = useState([]);
  const [visibleAlert, setVisibleAlert] = useState(false);

  const onDismiss = () => {
    setVisibleAlert(false);
    props.clearMessages();
  }

   useEffect(() => {
     if (props.message) {
      setVisibleAlert(true)
    }
  }, [props.message]);

  useEffect(() => {
    getNewList()
  }, [props.newTapActive])

const  getNewList = () => {
    if(props.newTapActive){
        props.getNewPosts()
    } 
}


   useEffect(() => {
    getPostsByIds(postIds, 'new')
  }, [postIds])

  useEffect(() => {
    
    setPostIds(slice(props.allPostsId, startIndex , limit + startIndex))
    setPostsLenght(props.allPostsId.length)
  }, [props.allPostsId])


    useEffect(() => {
    setNewPostsView( props.post )
  }, [props.post])


  

  const getPostsByIds = (postIds , type) => {
    if(postIds.length > 0){
      postIds.map( id => {
      props.getPostsById(id , type)
    })
    setStartInsdex((limit + startIndex + 1))
    }
  } 

  const loadMore = () =>{
    
    setPostIds(slice(props.allPostsId, startIndex , limit + startIndex))
    setStartInsdex((limit + startIndex + 1))
  }

   const renderNewPosts = (item , index) => {
    return(
      <Post key={index}  data={item} />
    )
  }

    return (
      <>  
                    <Alert color="danger" isOpen={visibleAlert} toggle={onDismiss}>
                      {props.message}
                    </Alert>
                     {newPostsView.length > 0 ? (  
                     <FlatList
                        list={newPostsView}
                        renderItem={renderNewPosts}
                        renderWhenEmpty={() => <div>List is empty!</div>}
                        
                      />) : 
                      <SkeletonTheme color="#F2F2F2" highlightColor="#fff" >
                        <p>
                          <Skeleton count={5}   height={100}/>
                        </p>
                      </SkeletonTheme>
                      }
                           {props.loading === true ? 
                          <Row className="justify-content-center">
                              <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" color="#000" />
                          </Row>
                        
                        :
                        null
                        } 
                
                      <Button className="load-more-btn" onClick={loadMore}> 
                        Load More
                      </Button>               
         
      </>
    );
}


const mapStateToProps = ({ postR }) => {
    const {
        loading,
        allPostsId,
        post,
        message
    } = postR;
    return {
        loading,
        allPostsId,
        post,
        message
    };
};
export default connect(mapStateToProps, {
    getNewPosts,
    getPostsById,
    clearMessages
})(memo(NewPostList));
