/* eslint-disable */
import React ,{memo, useEffect , useState} from "react";
import { connect } from 'react-redux';
import { getTopPosts, getPostsById, clearMessages} from '../../actions'
import FlatList from 'flatlist-react';
import {slice, concat} from 'lodash';
import Skeleton , { SkeletonTheme }from 'react-loading-skeleton';
import {
  Button,
  Spinner,
  Row,
  Alert
} from "reactstrap";
import Post from "./Post";




const TopPostList = (props) => {

  const limit = 5;
  const [setTopStartIndex , setTopStartInsdex ] = useState(0);
  const [postsLenght , setPostsLenght ] = useState(5);
  const [TopPostsView , setTopPostsView] = useState([])
  const[postIds , setPostIds] = useState([]);
  const [visibleAlert, setVisibleAlert] = useState(false);

  const onDismiss = () => {
    setVisibleAlert(false);
    props.clearMessages();
  }

   useEffect(() => {
     if (props.message) {
      setVisibleAlert(true)
    }
  }, [props.message]);


useEffect(() => {
    getTopList()
  }, [props.topTapActive])

const  getTopList = () => {
    if(props.topTapActive){
        props.getTopPosts()
    } 
}
   useEffect(() => {
    getPostsByIds(postIds, 'top')
    
  }, [postIds])


   useEffect(() => {
    setTopPostsView( props.topPost )
  }, [props.topPost])


 

    useEffect(() => {
    setPostIds(slice(props.topPosts, setTopStartIndex , limit + setTopStartIndex))
    setPostsLenght(props.topPosts.length)
  }, [props.topPosts])




  const getPostsByIds = (postIds , type) => {
    if(postIds.length > 0){
      postIds.map( id => {
      props.getPostsById(id , type)
    })
    setTopStartInsdex((limit + setTopStartIndex + 1))
    }
  } 


   const loadMoreTop = () => {
    setPostIds(slice(props.topPosts, setTopStartIndex , limit + setTopStartIndex))
   setTopStartInsdex((limit + setTopStartIndex + 1))
  }


  const renderTopPosts = (item , index) => {
    return(
      <Post key={index}  data={item}/>
    )
  }
              return (
                <>
                     <Alert color="danger" isOpen={visibleAlert} toggle={onDismiss} fade={true}>
                      {props.message}
                    </Alert>
                    {TopPostsView.length > 0 ? (
                         <FlatList
                        list={TopPostsView}
                        renderItem={renderTopPosts}
                        renderWhenEmpty={() => <div>List is empty!</div>}
                        sortBy={["id", {key: "id", descending: true}]}
                      />

                    ) :
                        <SkeletonTheme color="#F2F2F2" highlightColor="#fff" >
                        <p>
                          <Skeleton count={5}   height={100}/>
                        </p>
                      </SkeletonTheme>
                    }
                   

                           {props.loading === true ? 
                          <Row className="justify-content-center">
                              <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" color="#000" />
                          </Row>
                        
                        :
                        null
                        }
                      <Button className="load-more-btn" onClick={loadMoreTop}> 
                        Load More
                      </Button>
               
               </>
    );
}


const mapStateToProps = ({ postR }) => {
    const {
        loading,
        topPost,
        topPosts,
        topPostType,
        message
    } = postR;
    return {
        loading,
        topPost,
        topPosts,
        topPostType,
        message
    };
};
export default connect(mapStateToProps, {
    getTopPosts,
    getPostsById,
    clearMessages
})(memo(TopPostList));
