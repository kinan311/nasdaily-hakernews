/* eslint-disable */
import React ,{memo, useEffect , useState} from "react";
import { connect } from 'react-redux';
import { getPostsById, getBestPosts , clearMessages} from '../../actions'
import FlatList from 'flatlist-react';
import {slice} from 'lodash';
import Skeleton , { SkeletonTheme }from 'react-loading-skeleton';
import {
  Button,
  Spinner,
  Row,
  Alert
} from "reactstrap";
import Post from "./Post";

const BestPostList = (props) => {
  const limit = 5;
  const [bestStartIndex , setBestStartIndex ] = useState(0);
  const [postsLenght , setPostsLenght ] = useState(5);
  const [bestPostsView , setbestPostsView] = useState([])
  const[postIds , setPostIds] = useState([]);
  const [visibleAlert, setVisibleAlert] = useState(false);

  const onDismiss = () => {
    setVisibleAlert(false);
    props.clearMessages();
  }

useEffect(() => {
    getBestList()
  }, [props.bestTapActive])

const  getBestList = () => {
    if(props.bestTapActive){
        props.getBestPosts()
    } 
}
  
   useEffect(() => {
    getPostsByIds(postIds, 'best')
  }, [postIds])

    useEffect(() => {
    setbestPostsView( props.bestPost )
  }, [props.bestPost])

 

    useEffect(() => {
    setPostIds(slice(props.bestPosts, bestStartIndex , limit + bestStartIndex))
    setPostsLenght(props.bestPosts.length)
  }, [props.bestPosts])

  
  useEffect(() => {
     if (props.message) {
      setVisibleAlert(true)
    }
  }, [props.message]);


  const getPostsByIds = (postIds , type) => {
    if(postIds.length > 0){
      postIds.map( id => {
      props.getPostsById(id , type)
    })
    setBestStartIndex((limit + bestStartIndex + 1))
    }
  } 

  const loadMoreBest = () => {
     setPostIds(slice(props.bestPosts, bestStartIndex , limit + bestStartIndex))
    setBestStartIndex((limit + bestStartIndex + 1))
  }


  const renderbestPosts = (item , index) => {
     return(
      <Post key={index}  data={item}/>
    )
  }

    return (
      <>   

                    <Alert color="danger" isOpen={visibleAlert} toggle={onDismiss}>
                      {props.message}
                    </Alert>
                    {bestPostsView.length > 0 ? (
                         <FlatList
                        list={bestPostsView}
                        renderItem={renderbestPosts}
                        renderWhenEmpty={() => <div>List is empty!</div>}
                      />

                    ) :
                        <SkeletonTheme color="#F2F2F2" highlightColor="#fff" >
                        <p>
                          <Skeleton count={5}   height={100}/>
                        </p>
                      </SkeletonTheme>
                    }
                   

                           {props.loading === true ? 
                          <Row className="justify-content-center">
                              <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" color="#000" />
                          </Row>
                        
                        :
                        null
                        }
                      <Button className="load-more-btn" onClick={loadMoreBest}> 
                        Load More
                      </Button>

                     
                      
               
      </>
    );
}


const mapStateToProps = ({ postR }) => {
    const {
        loading,
        bestPost,
        bestPosts,
        bestPostType,
        message
    } = postR;
    return {
        loading,
        bestPost,
        bestPosts,
        bestPostType,
        message
    };
};
export default connect(mapStateToProps, {
    getPostsById,
    getBestPosts,
    clearMessages,
})(memo(BestPostList));
