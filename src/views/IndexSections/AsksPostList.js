/* eslint-disable */
import React ,{memo, useEffect , useState} from "react";
import { connect } from 'react-redux';
import { getPostsById, getAskstories , clearMessages} from '../../actions'
import FlatList from 'flatlist-react';
import {slice} from 'lodash';
import Skeleton , { SkeletonTheme }from 'react-loading-skeleton';
import {
  Button,
  Spinner,
  Row,
  Alert
} from "reactstrap";
import Post from "./Post";

const askPostList = (props) => {
  const limit = 5;
  const [askStartIndex , setaskStartIndex ] = useState(0);
  const [postsLenght , setPostsLenght ] = useState(5);
  const [askPostsView , setaskPostsView] = useState([])
  const[postIds , setPostIds] = useState([]);
  const [visibleAlert, setVisibleAlert] = useState(false);

  const onDismiss = () => {
    setVisibleAlert(false);
    props.clearMessages();
  }

useEffect(() => {
    getaskList()
  }, [props.askTapActive])

const  getaskList = () => {
    if(props.askTapActive){
        props.getAskstories()
    } 
}
  
   useEffect(() => {
    getPostsByIds(postIds, 'ask')
  }, [postIds])

    useEffect(() => {
    setaskPostsView( props.asksPost )
  }, [props.asksPost])

 

    useEffect(() => {
    setPostIds(slice(props.asksPosts, askStartIndex , limit + askStartIndex))
    setPostsLenght(props.asksPosts.length)
  }, [props.asksPosts])

  
  useEffect(() => {
     if (props.message) {
      setVisibleAlert(true)
    }
  }, [props.message]);


  const getPostsByIds = (postIds , type) => {
    if(postIds.length > 0){
      postIds.map( id => {
      props.getPostsById(id , type)
    })
    setaskStartIndex((limit + askStartIndex + 1))
    }
  } 

  const loadMoreask = () => {
    setPostIds(slice(props.asksPosts, askStartIndex , limit + askStartIndex))
    setaskStartIndex((limit + askStartIndex + 1))
  }


  const renderaskPosts = (item , index) => {
     return(
      <Post key={index}  data={item}/>
    )
  }

    return (
      <>   

                    <Alert color="danger" isOpen={visibleAlert} toggle={onDismiss}>
                      {props.message}
                    </Alert>
                    {askPostsView.length > 0 ? (
                         <FlatList
                        list={askPostsView}
                        renderItem={renderaskPosts}
                        renderWhenEmpty={() => <div>List is empty!</div>}
                      />

                    ) :
                        <SkeletonTheme color="#F2F2F2" highlightColor="#fff" >
                        <p>
                          <Skeleton count={5}   height={100}/>
                        </p>
                      </SkeletonTheme>
                    }
                   

                           {props.loading === true ? 
                          <Row className="justify-content-center">
                              <Spinner style={{ width: '3rem', height: '3rem' }} type="grow" color="#000" />
                          </Row>
                        
                        :
                        null
                        }
                      <Button className="load-more-btn" onClick={loadMoreask}> 
                        Load More
                      </Button>

                     
                      
               
      </>
    );
}


const mapStateToProps = ({ postR }) => {
    const {
        loading,
        asksPost,
        asksPosts,
        message
    } = postR;
    return {
        loading,
        asksPost,
        asksPosts,
        message
    };
};
export default connect(mapStateToProps, {
    getPostsById,
    getAskstories,
    clearMessages,
})(memo(askPostList));
