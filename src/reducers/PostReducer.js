import {

  CLEAR_MESSAGE,
  GET_POSTS,
  GET_POSTS_FAIL,
  GET_POSTS_SUCCESS,
  GET_POST_BY_ID,
  GET_POST_BY_ID_FAIL,
  GET_NEW_POST_BY_ID_SUCCESS,
  GET_BEST_POST_BY_ID_SUCCESS,
  GET_TOP_POST_BY_ID_SUCCESS,
  GET_SHOW_POST_BY_ID_SUCCESS,
  GET_JOB_POST_BY_ID_SUCCESS,
  GET_ASK_POST_BY_ID_SUCCESS,
  DONE_POST,
  GET_BEST_POSTS,
  GET_BEST_POSTS_SUCCESS,
  GET_BEST_POSTS_FAIL,
  GET_TOP_POSTS,
  GET_TOP_POSTS_SUCCESS,
  GET_TOP_POSTS_FAIL,
  NEW_POST_TYPE,
  TOP_POST_TYPE,
  BEST_POST_TYPE,
  GET_SHOW,
  GET_SHOW_SUCCESS,
  GET_SHOW_FAIL,
  GET_ASK_SUCCESS,
  GET_ASK_FAIL,
  GET_ASKS,
  GET_JOBS_SUCCESS,
  GET_JOBS_FAIL,
  GET_JOBS,
} from '../actions/types';

const INITAL_STATE = {
 
  allPostsId: [],
  post: [],
  loading: false,
  topPosts: [],
  bestPosts: [],
  jobPosts: [],
  asksPosts:[],
  showPosts: [],
  newPostType: '',
  topPostType: '',
  bestPostType: '',
  topPost: [],
  bestPost :[],
  jobPost: [],
  asksPost:[],
  showPost: [],
  message: ''
  // checkCoupon: '',
};
export default (state = INITAL_STATE, action) => {
  switch (action.type) {
    case CLEAR_MESSAGE:
      return {...state ,  message: ''};
    case GET_POSTS:
      return {...state, loading: true , allPostsId: []};
    case GET_POSTS_SUCCESS:
      return {...state, loading: false, allPostsId: action.payload};
    case GET_POSTS_FAIL:
      return {...state, loading: false ,  message: action.payload};
      case DONE_POST: 
        return {
           ...state,
        posts: []
        }

    case NEW_POST_TYPE: 
        return {
           ...state,
        newPostType: action.payload
        }

    case TOP_POST_TYPE: 
        return {
           ...state,
        topPostType: action.payload
        }
   case BEST_POST_TYPE: 
        return {
           ...state,
        bestPostType: action.payload
        }
        
    case GET_POST_BY_ID:
      return {...state, loading: true,};
    case GET_NEW_POST_BY_ID_SUCCESS:
      return {...state, loading: false, post: [...state.post , action.payload],   bestPost: [] , topPost: [], jobPost: [], showPost: [] ,asksPost:[]};
    
    case GET_BEST_POST_BY_ID_SUCCESS:
      return {...state, loading: false, bestPost:[...state.bestPost , action.payload], post: [] , topPost: [], jobPost: [], showPost: [] ,asksPost:[] };
    

      case GET_TOP_POST_BY_ID_SUCCESS:
      return {...state, loading: false,  topPost: [...state.topPost , action.payload], post: [], bestPost: [], jobPost: [], showPost: [] ,asksPost:[]};

      case GET_JOB_POST_BY_ID_SUCCESS:
      return {...state, loading: false,  jobPost: [...state.jobPost , action.payload],post: [], bestPost: [], topPost: [], showPost: [] ,asksPost:[]};


      case GET_SHOW_POST_BY_ID_SUCCESS:
      return {...state, loading: false,  showPost: [...state.showPost , action.payload],post: [], bestPost: [], topPost: [], jobPost: [] ,asksPost:[]};

      case GET_ASK_POST_BY_ID_SUCCESS:
      return {...state, loading: false, asksPost: [...state.asksPost , action.payload],post: [], bestPost: [], topPost: [], showPost: [] ,jobPost:[]};
    
      case GET_POST_BY_ID_FAIL:
      return {...state, loading: false, message: action.payload};


     case GET_BEST_POSTS:
      return {...state, loading: true , bestPosts: []};
    case GET_BEST_POSTS_SUCCESS:
      return {...state, loading: false, bestPosts: action.payload};
    case GET_BEST_POSTS_FAIL:
      return {...state, loading: false ,  message: action.payload};

    case GET_TOP_POSTS:
      return {...state, loading: true , topPosts: []};
    case GET_TOP_POSTS_SUCCESS:
      return {...state, loading: false, topPosts: action.payload};
    case GET_TOP_POSTS_FAIL:
      return {...state, loading: false ,  message: action.payload};


      case GET_ASKS:
      return {...state, loading: true , asksPosts: []};
    case GET_ASK_SUCCESS:
      return {...state, loading: false, asksPosts: action.payload};
    case GET_ASK_FAIL:
      return {...state, loading: false ,  message: action.payload};


        case GET_SHOW:
      return {...state, loading: true , showPosts: []};
    case GET_SHOW_SUCCESS:
      return {...state, loading: false, showPosts: action.payload};
    case GET_SHOW_FAIL:
      return {...state, loading: false ,  message: action.payload};


    case GET_JOBS:
      return {...state, loading: true , jobPosts: []};
    case GET_JOBS_SUCCESS:
      return {...state, loading: false, jobPosts: action.payload};
    case GET_JOBS_FAIL:
      return {...state, loading: false ,  message: action.payload};

    default:
      return state;
  }
};
