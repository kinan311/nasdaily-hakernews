import {
 CLEAR_MESSAGE,
  GET_POSTS,
  GET_POSTS_FAIL,
  GET_POSTS_SUCCESS,
   GET_POST_BY_ID,
  GET_POST_BY_ID_FAIL,
  GET_NEW_POST_BY_ID_SUCCESS,
  GET_BEST_POST_BY_ID_SUCCESS,
  GET_TOP_POST_BY_ID_SUCCESS,
   GET_SHOW_POST_BY_ID_SUCCESS,
  GET_JOB_POST_BY_ID_SUCCESS,
  GET_ASK_POST_BY_ID_SUCCESS,
  DONE_POST,
  NEW_POST_TYPE,
  TOP_POST_TYPE,
  BEST_POST_TYPE,
  GET_BEST_POSTS,
  GET_BEST_POSTS_SUCCESS,
  GET_BEST_POSTS_FAIL,
  GET_JOBS_SUCCESS,
  GET_JOBS_FAIL,
  GET_JOBS,
  GET_SHOW,
  GET_SHOW_SUCCESS,
  GET_SHOW_FAIL,
  GET_ASK_SUCCESS,
  GET_ASK_FAIL,
  GET_ASKS,
  GET_TOP_POSTS,
  GET_TOP_POSTS_SUCCESS,
  GET_TOP_POSTS_FAIL,
} from './types';
import axios from 'axios';
// import {headers,  L} from '../Config';
import {uniqBy} from 'lodash'


const headers =   {
      // 'Authorization':  "Basic " + base64.encode('admin' + ':' + '123'),
      'Accept': 'application/json',
      'Content-Type': 'application/json; charset=UTF-8'
    }
export const clearMessages = (text) => {
  return {
    type: CLEAR_MESSAGE,
    payload: text
  };
};

export const getNewPosts = () => {
  return dispatch => {
    dispatch({type: GET_POSTS});
    axios
      .get('https://hacker-news.firebaseio.com/v0/newstories.json')
      .then(function(data) {
          
        if (data.status === 200) {
          getNewPostsSuccess(dispatch, data.data);
        } else {
        
          getNewPostsFail(dispatch, data.err);
        }
      })
      .catch(function(error) {
  
        getNewPostsFail(dispatch, 'connectionError');
      });
  };
};

const getNewPostsFail = (dispatch, error) => {
  dispatch({
    type: GET_POSTS_FAIL,
    payload: error,
  });
};

const getNewPostsSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_POSTS_SUCCESS,
    payload: data,
  });
 
};

export const donePost = (text) => {
  return dispatch => dispatch({
    type: DONE_POST,
    payload: text
  });
};


export const newPostType = (text) => {
  return dispatch => dispatch({
    type: NEW_POST_TYPE,
    payload: text
  });
};

export const topPostType = (text) => {
  return dispatch => dispatch({
    type: TOP_POST_TYPE,
    payload: text
  });
};


export const bestPostType = (text) => {
  return dispatch => dispatch({
    type: BEST_POST_TYPE,
    payload: text
  });
};


export const getPostsById = (postId , type) => {
  return dispatch => {
    dispatch({type: GET_POST_BY_ID});
    axios
      .get(`https://hacker-news.firebaseio.com/v0/item/${postId}.json`)
      .then(function(data) {
          
        if (data.status === 200) {
          switch (type) {
            case 'new':
              getNewPostByIdsSuccess(dispatch, data.data);
              break;
            case 'best' :
              getBestPostByIdsSuccess(dispatch, data.data);
              break;
            case 'top':
              getTopPostByIdsSuccess(dispatch, data.data);
              break;

              case 'ask':
              getAskPostByIdsSuccess(dispatch, data.data);
              break;
             case 'job':
              getJobPostByIdsSuccess(dispatch, data.data);
              break;
             case 'show':
              getShowPostByIdsSuccess(dispatch, data.data);
              break;
            default : 
            getNewPostByIdsSuccess(dispatch, data.data);
            break;
          }
          
        } else {
        
          getPostByIdFail(dispatch, data.err);
        }
      })
      .catch(function(error) {
  
        getPostByIdFail(dispatch, 'connectionError');
      });
  };
};

const getPostByIdFail = (dispatch, error) => {
  dispatch({
    type: GET_POST_BY_ID_FAIL,
    payload: error,
  });
};

const getNewPostByIdsSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_NEW_POST_BY_ID_SUCCESS,
    payload: data,
  });
 
};


const getBestPostByIdsSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_BEST_POST_BY_ID_SUCCESS,
    payload: data,
  });
 
};


const getTopPostByIdsSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_TOP_POST_BY_ID_SUCCESS,
    payload: data,
  });
 
};


const getAskPostByIdsSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_ASK_POST_BY_ID_SUCCESS,
    payload: data,
  });
 
};


const getJobPostByIdsSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_JOB_POST_BY_ID_SUCCESS,
    payload: data,
  });
 
};


const getShowPostByIdsSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_SHOW_POST_BY_ID_SUCCESS,
    payload: data,
  });
 
};

export const getBestPosts = () => {
  return dispatch => {
    dispatch({type: GET_BEST_POSTS});
    axios
      .get('https://hacker-news.firebaseio.com/v0/beststories.json')
      .then(function(data) {
          
        if (data.status === 200) {
          getBestPostsSuccess(dispatch, data.data);
        } else {
        
          getBestPostsFail(dispatch, data.err);
        }
      })
      .catch(function(error) {
  
        getBestPostsFail(dispatch, 'connectionError');
      });
  };
};

const getBestPostsSuccess = (dispatch, error) => {
  dispatch({
    type: GET_BEST_POSTS_SUCCESS,
    payload: error,
  });
};

const getBestPostsFail = (dispatch, data) => {
 
  dispatch({
    type: GET_BEST_POSTS_FAIL,
    payload: data,
  });
 
};



export const getTopPosts = () => {
  return dispatch => {
    dispatch({type: GET_TOP_POSTS});
    axios
      .get('https://hacker-news.firebaseio.com/v0/topstories.json')
      .then(function(data) {
          
        if (data.status === 200) {
          getTopPostsSuccess(dispatch, data.data);
        } else {
        
          getTopPostsFail(dispatch, data.err);
        }
      })
      .catch(function(error) {
  
        getTopPostsFail(dispatch, 'connectionError');
      });
  };
};

const getTopPostsFail = (dispatch, error) => {
  dispatch({
    type: GET_TOP_POSTS_FAIL,
    payload: error,
  });
};

const getTopPostsSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_TOP_POSTS_SUCCESS,
    payload: data,
  });
 
};



export const getAskstories = () => {
  return dispatch => {
    dispatch({type: GET_ASKS});
    axios
      .get('https://hacker-news.firebaseio.com/v0/askstories.json')
      .then(function(data) {
          
        if (data.status === 200) {
          getAskstoriesSuccess(dispatch, data.data);
        } else {
        
           getAskstoriesFail(dispatch, data.err);
        }
      })
      .catch(function(error) {
  
         getAskstoriesFail(dispatch, 'connectionError');
      });
  };
};

const getAskstoriesFail = (dispatch, error) => {
  dispatch({
    type: GET_ASK_FAIL,
    payload: error,
  });
};

const getAskstoriesSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_ASK_SUCCESS,
    payload: data,
  });
 
};



export const getJobStories = () => {
  return dispatch => {
    dispatch({type: GET_JOBS});
    axios
      .get('https://hacker-news.firebaseio.com/v0/jobstories.json')
      .then(function(data) {
          
        if (data.status === 200) {
          getJobStoriesSuccess(dispatch, data.data);
        } else {
        
           getJobStoriesFail(dispatch, data.err);
        }
      })
      .catch(function(error) {
  
         getJobStoriesFail(dispatch, 'connectionError');
      });
  };
};

const getJobStoriesFail = (dispatch, error) => {
  dispatch({
    type: GET_JOBS_FAIL,
    payload: error,
  });
};

const getJobStoriesSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_JOBS_SUCCESS,
    payload: data,
  });
 
};



export const getShowStories = () => {
  return dispatch => {
    dispatch({type: GET_SHOW});
    axios
      .get('https://hacker-news.firebaseio.com/v0/showstories.json')
      .then(function(data) {
          
        if (data.status === 200) {
          getShowStoriesSuccess(dispatch, data.data);
        } else {
        
           getShowStoriesFail(dispatch, data.err);
        }
      })
      .catch(function(error) {
  
         getShowStoriesFail(dispatch, 'connectionError');
      });
  };
};

const getShowStoriesFail = (dispatch, error) => {
  dispatch({
    type: GET_SHOW_FAIL,
    payload: error,
  });
};

const getShowStoriesSuccess = (dispatch, data) => {
 
  dispatch({
    type: GET_SHOW_SUCCESS,
    payload: data,
  });
 
};




